
[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  

|[บทที่ 1](ch1TH.md)| [หน้าหลัก](../../README.md) | [Contents](../contentEN.md) | [สารบัญ](../contentTH.md) |
| ---------- | ---------- | ---------- | -------- |   

|[ก่อนหน้า](ch1-07-02TH.md)| [ต่อไป](ch1-08TH.md) |
| ---------- | ---------- |    

### 1.7.3 [ตัวอย่างการหาปริมาตรครึ่งทรงกลม](ch1-07-03TH.md)  
จงหาปริมาตรของครึ่งทรงกลมที่มีรัศมีเท่ากับ $`a`$  
#### วิธีทำ  
สมการพื้นผิวทรงกลมที่มีจุดศูนย์กลางอยู่ที่จุดกำเนิดและมีรัศมีเท่ากับ $`a`$ คือ $`x^2+y^2+z^2=a^2`$ หรือเขียนในพิกัดทรงกระบอกโดยแทนค่า $`r^2 = x^2+y^2+z^2`$ ได้ดังนี้ $`r^2=a^2`$ หรือ พื้นผิวครึ่งทรงกลมคือ $`r= a`$  
  
เราให้ปริมาตรดิฟเฟอเรนเชียล หรือปริมาตรที่เล็กที่สุด $`dv = r^2 \sin \theta dr \theta d\phi `$ ซึ่งมาจากการเอาด้านสามด้านของปริมาตรดิฟเฟอเรนเชียลในพิกัดทรงกลมคือ $`dr`$ $`r d \theta`$ และ $`r \sin \theta d \phi`$ มาคูณกัน  
  
ถ้าเราแบ่งปริมาตรของครึ่งทรงกลมเป็นกล่องสี่เหลี่ยมเล็กๆ โดยให้ขนาดของกล่องเท่ากับ $`\Delta v_m = r_i^2 \Delta r_i \sin \theta_j \Delta \theta_j \phi_k`$ ซึ่งถ้าเราให้ขนาดของ $`\Delta r_i`$ $`\Delta \theta_j`$ และ $`\Delta \phi_k`$ ของแต่ละกล่องมีขนาดเท่ากัน คือ $`\Delta r`$ $`\Delta \theta`$ และ $`\Delta \phi`$ และ $`r_i`$ คือระยะจากจุดกำเนิดไปยังศูนย์กลางของกล่อง และ $`\theta_j`$ คือมุมที่แกน $`r_i`$ ซึ่งชี้ไปยังศูนย์กลางของกล่อง ทำกับแกน $`z`$ แล้วเอาปริมาตรของกล่องสี่เหลี่ยมแต่ละชิ้นมาร่วมกันทั้งหมด เราจะได้ปริมาตรของครึ่งทรงกลมโดยประมาณ
```math
V \approx \sum_{m=1}^{N} {\Delta v_m} = \sum_{i=1}^{i=N_r} \sum_{j=\theta_{start(i)}}^{j= \theta_{stop(i)}} \sum_{k=\phi_{start(i,j)}}^{k= \phi_{stop(i,j)}}   { \Delta \phi_k \sin \theta_j \Delta \theta_j r_i^2 \Delta r_i}
```   
ค่าของ $`\phi_{start}`$ และ $`\phi_{stop}`$ อาจขึ้นอยู่กับค่า $`i`$ และ $`j`$ แต่ข้อนี้มุม $`\phi`$ ไม่ขึ้นกับค่า $`r`$ และ $`\theta`$ แต่เป็นวงกลมคือ $`\phi_{start}=0`$ และ $`\phi_{stop}= 2\pi`$ และ $`\theta_{start}`$ และ $`\theta_{stop}`$ อาจขึ้นอยู่กับค่า $`i`$ แต่ข้อนี้มุม $`\theta`$ ไม่ขึ้นกับค่า $`r`$ แต่เป็นหนึ่งส่วนสี่ของวงกลมคือ $`\theta_{start}=0`$ และ $`\theta_{stop}= \frac{\pi}{2}`$ เราจะได้   
```math
V \approx \sum_{m=1}^{N} {\Delta v_m} = \sum_{i=1}^{i=N_r} \sum_{j=(\theta=0)}^{j= \left(\theta=\frac{\pi}{2}\right)}  \sum_{k=(\phi=0)}^{k= (\phi = 2\pi)} { \Delta \phi_k \sin \theta_j \Delta \theta_j r_i^2 \Delta r_i}  
```   
จะเห็นว่างแต่ละตัวแปรไม่เกี่ยวของกันเลยเราสามารถแยกเป็นสามเทอมได้ดังนี้  
```math
V \approx \sum_{i=1}^{i=N_r} {r_i^2 \Delta r_i} \sum_{j=(\theta=0)}^{j= \left(\theta=\frac{\pi}{2}\right)} {\sin \theta_j \Delta \theta_j}  \sum_{k=(\phi=0)}^{k= (\phi = 2\pi)} { \Delta \phi_k  }  
```   
```math
V \approx \sum_{i=1}^{i=N_r} {r_i^2 \Delta r_i} \sum_{j=1}^{j= N_\theta} {\sin \theta_j \Delta \theta_j}  \sum_{k=1}^{k= N} { \Delta \phi_k  }  
``` 
- #### หาค่าของเทอม $`\Delta r `$   
  
ถ้าให้ $`N_r = 4`$ เราจะได้ว่า $`\Delta r_i = \Delta r = \frac{a}{4}`$ และ $` r_i = \Delta r \left(i -0.5\right)`$ เราจะได้  
```math
\sum_{i=1}^{i=N_r} {r_i^2 \Delta r_i} = \sum_{i=1}^{i=4} {(\Delta r \left(i -0.5\right) )^2 \Delta r}
```   
```math
\sum_{i=1}^{i=N_r} {r_i^2 \Delta r_i} = \sum_{i=1}^{i=4} { \left(i -0.5\right)^2 \Delta r^3}
```   
```math
\sum_{i=1}^{i=N_r} {r_i^2 \Delta r_i} = \sum_{i=1}^{i=4} { \left(i -0.5\right)^2 \frac{a^3}{4^3}}
```   
```math
\sum_{i=1}^{i=N_r} {r_i^2 \Delta r_i} = \frac{a^3}{4^3} \sum_{i=1}^{i=4} { \left(i -0.5\right)^2 }
```   
```math
\sum_{i=1}^{i=N_r} {r_i^2 \Delta r_i} = \frac{a^3}{4^3} { \left(\left(0.5\right)^2+\left(1.5\right)^2+\left(2.5\right)^2+\left(3.5\right)^2\right) }
```   
เราสามารถหาค่า $`\sum_{i=1}^{i=4} (i-0.5)  `$ ได้จากเว็บไซต์ [octave-online](https://octave-online.net/) โดยพิมพ์คำสั่งตามด้านล่างซึ่งจะได้คำตอบ $`\sum_{i=1}^{i=4} (i-0.5)  = 21`$  
```
octave:1> sum(([1 2 3 4] - 0.5).^2)
ans = 21
``` 
```math
\sum_{i=1}^{i=N_r} {r_i^2 \Delta r_i} = \frac{21 a^3}{64} 
```  
- #### หาค่าของเทอม $`\Delta \theta `$   
  
ถ้าให้ $`N_\theta = 4`$ เราจะได้ว่า $`\Delta \theta_j = \Delta \theta = \frac{0.5 \pi}{4} = \frac{\pi}{8} `$ และ $` \theta_j = \Delta \theta \left(i -0.5\right)`$ เราจะได้ 
```math
\sum_{j=1}^{j= N_\theta} {\sin \theta_j \Delta \theta_j} = \sum_{j=1}^{j= 4} {\sin \left(\Delta \theta \left(i -0.5\right)\right) \Delta \theta}    
```
```math
\sum_{j=1}^{j= N_\theta} {\sin \theta_j \Delta \theta_j} = \sum_{j=1}^{j= 4} {\sin \left(\frac{\pi}{8} \left(i -0.5\right)\right) \frac{\pi}{8}}    
```
เราสามารถหาค่า $`\sum_{j=1}^{j= 4} {\sin \left(\frac{\pi}{8} \left(i -0.5\right)\right) \frac{\pi}{8}}  `$ ได้จากเว็บไซต์ [octave-online](https://octave-online.net/) โดยพิมพ์คำสั่งตามด้านล่างซึ่งจะได้คำตอบ $`\sum_{j=1}^{j= 4} {\sin \left(\frac{\pi}{8} \left(i -0.5\right)\right) \frac{\pi}{8}}  = 1.0065`$  
```
octave:1> sum(sin((pi/8)*([1 2 3 4] - 0.5))*(pi/8))
ans = 1.0065
``` 
- #### หาค่าของเทอม $`\Delta \phi `$   
  
ถ้าให้ $`N_\phi = N`$ เราจะได้ว่า $`\Delta \phi_k = \Delta \phi = \frac{2 \pi}{N} `$ เราจะได้  
```math
\sum_{k=1}^{k= N} { \Delta \phi_k  } = \sum_{k=1}^{k= N} { \Delta \phi  }
```
```math
\sum_{k=1}^{k= N} { \Delta \phi_k  } = \sum_{k=1}^{k= N} { \frac{2\pi}{N}  }
```
```math
\sum_{k=1}^{k= N} { \Delta \phi_k  } = 2\pi  
```
- #### รวมค่าที่ได้ทั้งสามเทอม  
```math
V \approx \sum_{i=1}^{i=N_r} {r_i^2 \Delta r_i} \sum_{j=1}^{j= N_\theta} {\sin \theta_j \Delta \theta_j}  \sum_{k=1}^{k= N} { \Delta \phi_k  }  
```    
```math
V \approx \frac{21 a^3}{64} \times 1.0065 \times 2 \pi  
```    
```math
V \approx \frac{42.273 \pi a^3}{64}   
```  
```math
V \approx 0.6605 \pi a^3   
```  
  
ค่าจริงของปริมาตรคือ $`\frac{2}{3} \pi a^3`$ หรือ $`0.67 \pi a^3`$ ซึ่งมีความผิดพลาดประมาณ 0.3% 
ปริมาตรที่ได้จากการประมาณจะต่างจากค่าจริงเพราะกล่องสี่เหลี่ยมไม่สามารถเก็บรายละเอียดของผิวโค้งได้ ผิวโค้งจะมีขรุขระเป็นขั้นบันได ถ้าจะให้ได้ค่าปริมาตรที่ใกล้เคียงกับค่าจริงมากขึ้นเราต้องแบ่ง $`\Delta r`$ $`\Delta \theta`$ และ $`\Delta \phi`$ ให้มีขนาดที่เล็กลง ถ้า $`\Delta r`$ $`\Delta \theta`$ และ $`\Delta \phi`$ มีขนาดที่เล็กที่สุดที่จะเล็กได้ เราจะได้ขนาดของปริมาตรจริง แต่เราต้องรวมปริมาตรของกล่อมซึ่งมีจำนวนเป็นอนั้นต์   
```math
V = \sum_{i=1}^{i=\infty} {r_i^2 \Delta r_i} \sum_{j=1}^{j= \infty} {\sin \theta_j \Delta \theta_j}  \sum_{k=1}^{k= \infty} { \Delta \phi_k  }  
```    
ซึ่งเราไม่สามารถเอาค่ามารวมกันเป็นอนันต์ (infinite) ครั้ง แต่เราจะเปลี่ยนเครื่องหมาย summation เป็นเครื่องหมาย integrate แทนและเปลี่ยน $`{\Delta r_i}`$ เป็น $`{dr}`$ และ $`{\Delta \theta_j}`$ เป็น $`{d\theta}`$ และ $`{\Delta \phi_j}`$ เป็น $`{d\phi}`$ ดังนั้น   
```math
V = \sum_{i=1}^{i=\infty} {r_i^2 \Delta r_i} \sum_{j=1}^{j= \infty} {\sin \theta_j \Delta \theta_j}  \sum_{k=1}^{k= \infty} { \Delta \phi_k  }   
```    
```math
V =  \int_{r=0}^{r=a} {r^2 dr} \int_{\theta=0}^{\theta= \frac{\pi}{2}} {\sin \theta d\theta}  \int_{\phi=0}^{\phi= 2 \pi} { d\phi  }  
```    
```math
V =   \frac{r^3}{3}\bigg|_{r=0}^{r=a}  {\left(-\cos \theta \right)}\bigg|_{\theta=0}^{\theta= \frac{\pi}{2}}   { \phi  }\bigg|_{\phi=0}^{\phi= 2 \pi}  
```    
```math
V =   \frac{a^3}{3}{\left(\cos 0 - \cos \frac{\pi}{2}\right)}   { 2 \pi  }  
```    
```math
V =   \frac{2\pi a^3}{3} 
```    
  
ตรงตามที่เราเรียนมา จะเห็นว่าสมการอินทิเกรตดูง่ายกว่าการหาค่าโดยใช้[พิกัดฉาก](ch1-03-03TH.md) และ[พิกัดทรงกระบอก](ch1-06-03TH.md)มากเพราะเราใช้พิกัดตรงตามรูปแบบของปัญหา ดังนั้นเราควรพิจารณาปัญหาว่าพิกัดแบบใดเหมาะกับปัญหานั้นๆ   
|[ก่อนหน้า](ch1-07-02TH.md)| [ต่อไป](ch1-08TH.md) |
| ---------- | ---------- | 

[$`\footnotesize \copyright 2020 \: \text {Denchai Worasawate}`$](https://www.researchgate.net/profile/Denchai-Worasawate)  


